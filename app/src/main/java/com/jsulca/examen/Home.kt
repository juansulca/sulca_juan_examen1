package com.jsulca.examen

import android.content.Context
import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.navigation.findNavController


class Home : Fragment() {
    // TODO: Rename and change types of parameters
    private var listener: OnFragmentInteractionListener? = null
    private lateinit var addButton: Button
    private lateinit var subButton: Button
    private lateinit var multButton: Button
    private lateinit var divButton: Button

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        addButton = view.findViewById(R.id.button_additions)
        addButton.setOnClickListener { goToAdditions() }
        subButton = view.findViewById(R.id.button_substraction)
        subButton.setOnClickListener { goToSubstraction() }
        multButton = view.findViewById(R.id.button_multiplication)
        multButton.setOnClickListener { goToMultiplication() }
        divButton = view.findViewById(R.id.button_division)
        divButton.setOnClickListener { goToDivision() }
    }

    fun goToAdditions(){
        val action =  HomeDirections.actionHomeToOpperations(getString(R.string.plus))
        view?.findNavController()?.navigate(action)
    }

    fun goToSubstraction(){
        val action =  HomeDirections.actionHomeToOpperations(getString(R.string.minus))
        view?.findNavController()?.navigate(action)
    }

    fun goToMultiplication(){
        val action =  HomeDirections.actionHomeToOpperations(getString(R.string.multiply))
        view?.findNavController()?.navigate(action)
    }

    fun goToDivision(){
        val action =  HomeDirections.actionHomeToOpperations(getString(R.string.divide))
        view?.findNavController()?.navigate(action)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {

        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    // TODO: Rename method, update argument and hook method into UI event
    fun onButtonPressed(uri: Uri) {
        listener?.onFragmentInteraction(uri)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     *
     *
     * See the Android Training lesson [Communicating with Other Fragments]
     * (http://developer.android.com/training/basics/fragments/communicating.html)
     * for more information.
     */
    interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        fun onFragmentInteraction(uri: Uri)
    }
}
