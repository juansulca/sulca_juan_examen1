package com.jsulca.examen

import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.os.CountDownTimer
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.navigation.fragment.navArgs
import kotlin.random.Random

class Opperations : Fragment() {
    private var listener: OnFragmentInteractionListener? = null

    private lateinit var roundText: TextView
    private lateinit var scoreText: TextView
    private lateinit var timeLeftText: TextView
    private lateinit var firstNumber: TextView
    private lateinit var secondNumber: TextView
    private lateinit var opperand: TextView
    private lateinit var result: TextView
    private lateinit var buttonSend: Button

    val args: OpperationsArgs by navArgs()

    var round = 1
    var score = 0
    var timeleft = 10

    var num1 = 0
    var num2 = 0

    private var initialCountDown: Long = 10000
    private var countDownInterval: Long = 1000
    private lateinit var countDownTimer: CountDownTimer

    var countDownIsRunning = false


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        roundText = view.findViewById(R.id.game_round)
        opperand = view.findViewById(R.id.sign)
        scoreText = view.findViewById(R.id.game_score)
        timeLeftText = view.findViewById(R.id.time_left)
        firstNumber = view.findViewById(R.id.first_opperator)
        secondNumber = view.findViewById(R.id.second_opperator)
        result = view.findViewById(R.id.user_result)
        buttonSend = view.findViewById(R.id.button_send)
        val opp = args.opperation
        opperand.text = opp
        buttonSend.setOnClickListener { sendResult() }
        score = 0
        round = 0
        roundText.text = getString(R.string.round, round)
        timeLeftText.text = getString(R.string.time_left, timeleft)
        scoreText.text = getString(R.string.score, score)
        startRound()
    }


    fun startRound(){
        if(round <= 5){
            roundText.text = getString(R.string.round, round)
            timeleft = 10
            num1 = Random.nextInt(1, 10)
            num2 = Random.nextInt(1, 10)
            firstNumber.text = getString(R.string.first_opperator, num1)
            secondNumber.text = getString(R.string.second_opperator, num2)
            countDownTimer = object: CountDownTimer(initialCountDown, countDownInterval){
                override fun onFinish() {

                    countDownIsRunning = false
                    sendResult()

                }

                override fun onTick(millisUntilFinished: Long) {
                    timeleft = millisUntilFinished.toInt() / 1000
                    timeLeftText.text = getString(R.string.time_left, timeleft)
                    countDownIsRunning = true
                }

            }
            countDownTimer.start()
        } else {
            roundText.text = getString(R.string.round, round-1)
            Toast.makeText(activity, getString(R.string.final_score, score), Toast.LENGTH_LONG).show()
            buttonSend.isClickable = false

        }

        result.text = ""
    }

    fun sendResult(){
        if(countDownIsRunning) countDownTimer.cancel()
        val opp = opperand.text
        if(result.text.toString().isNotBlank()) {
            var res = result.text.toString().toDouble()
            var newScore = checkTime()
            if (opp == getString(R.string.plus)) {
                if (res == (num1 + num2).toDouble()) {
                    score += newScore
                } else {
                    newScore = 0
                }
            } else if (opp == getString(R.string.minus)) {
                if (res == (num1 - num2).toDouble()) {
                    score += checkTime()
                } else {
                    newScore = 0
                }
            } else if (opp == getString(R.string.multiply)) {
                if (res == (num1 * num2).toDouble()) {
                    score += newScore
                } else {
                    newScore = 0
                }
            } else if (opp == getString(R.string.divide)) {
                if (res == String.format("%.2f", (num1.toDouble() / num2.toDouble())).toDouble()) {
                    score += newScore
                } else {
                    newScore = 0
                }
            }

            Toast.makeText(activity, getString(R.string.score_msg, newScore), Toast.LENGTH_SHORT).show()
            scoreText.text = getString(R.string.score, score)
        }
        round += 1
        startRound()
    }

    fun checkTime(): Int{
        if(timeleft < 10 && timeleft >= 8){
            return 100
        } else if(timeleft >= 5){
            return 50
        } else {
            return 10
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {

        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_opperations, container, false)
    }

    // TODO: Rename method, update argument and hook method into UI event
    fun onButtonPressed(uri: Uri) {
        listener?.onFragmentInteraction(uri)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
        countDownTimer.cancel()
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     *
     *
     * See the Android Training lesson [Communicating with Other Fragments]
     * (http://developer.android.com/training/basics/fragments/communicating.html)
     * for more information.
     */
    interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        fun onFragmentInteraction(uri: Uri)
    }
}
